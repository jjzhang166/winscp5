The following is a copy of post at
https://forums.embarcadero.com/message.jspa?messageID=175480#175480

1) copy $(BCB6)\source\mfc to $(BDS)\source\mfc

2) copy $(BCB6)\include\mfc to $(BDS)\include\mfc

3) make following changes to files in $(BDS)\source\mfc

  - change line 64 of borland.mak to read:
    UNICODE=1

  - change line 140 of borland.mak to read:
    MODEL=U

  - add the following line to dumpstak.cpp at line 141
    TARGDEFS=/D_UNICODE /DUNICODE

  - change line 547 of borland.mak to read:
    $(LIBDIR)\$(GOAL).lib: $(D)\$(OBJS)

  - change line 548 of borland.mak to read:
    # @-if exist $@ erase $@

  - comment out line 82 of atldbcli.h to read:
    // DEFINE_OLEDB_TYPE_FUNCTION(DBFILETIME, DBTYPE_DBFILETIME)

  - change line 305 of atldbcli.h to read:
    #else // was #elseif

  - change line 775 of arccode.cpp to read:
    AfxThrowArchiveException(CArchiveException::generic);

  - change line 781 of arccode.cpp to read:
    AfxThrowArchiveException(CArchiveException::generic);

  - delete contents of bartool.cpp

  - delete contents of ctlcore.cpp

  - delete contents of ctlnownd.cpp

  - delete contents of dockcont.cpp

  - delete contents of dockstat.cpp

  - change code of following methods in inet.cpp
    CInternetSession::SetCookie
    CInternetSession::GetCookie (both overloads)
    to read
    AfxThrowInternetException(GetLastError());
    return false;

  - change code of following method in inet.cpp
    CInternetSession::GetCookieLength
    to read
    AfxThrowInternetException(GetLastError());
    return 0;

  - delete contents of isapimix.cpp

  - change line 281 of olecli1.cpp to read:
    LPCTSTR pstrIndex = _tcsinc(pstrSource);

  - add the following line to dumpstak.cpp at line 432
    #define _tclen(__a)         (1)

  - delete contents of viewprev.cpp

  - add the following lines to dumpstak.cpp, beginning at line 29
    #ifdef __BORLANDC__
    #define SYMBOL_INFO SYMBOL_INFO_LOCAL
    #endif

4) compile the mfc library using the command:
MAKE -fborland.mak NO_WARNINGS=1

By default the library will be at $(BDS)\lib\UafxcW.lib

See also:
https://forums.embarcadero.com/thread.jspa?messageID=175481&tstart=0
