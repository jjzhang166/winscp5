// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Customunixdirview.pas' rev: 21.00

#ifndef CustomunixdirviewHPP
#define CustomunixdirviewHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Messages.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Dialogs.hpp>	// Pascal unit
#include <Comctrls.hpp>	// Pascal unit
#include <Nortonlikelistview.hpp>	// Pascal unit
#include <Ielistview.hpp>	// Pascal unit
#include <Customdirview.hpp>	// Pascal unit
#include <Listviewcolproperties.hpp>	// Pascal unit
#include <Unixdirviewcolproperties.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Customunixdirview
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TCustomUnixDirView;
class PASCALIMPLEMENTATION TCustomUnixDirView : public Customdirview::TCustomDirView
{
	typedef Customdirview::TCustomDirView inherited;
	
private:
	void __fastcall SetUnixColProperties(Unixdirviewcolproperties::TUnixDirViewColProperties* Value);
	Unixdirviewcolproperties::TUnixDirViewColProperties* __fastcall GetUnixColProperties(void);
	
protected:
	virtual Listviewcolproperties::TCustomListViewColProperties* __fastcall NewColProperties(void);
	virtual bool __fastcall SortAscendingByDefault(int Index);
	
public:
	__property Items;
	
__published:
	__property Unixdirviewcolproperties::TUnixDirViewColProperties* UnixColProperties = {read=GetUnixColProperties, write=SetUnixColProperties};
public:
	/* TCustomDirView.Create */ inline __fastcall virtual TCustomUnixDirView(Classes::TComponent* AOwner) : Customdirview::TCustomDirView(AOwner) { }
	/* TCustomDirView.Destroy */ inline __fastcall virtual ~TCustomUnixDirView(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TCustomUnixDirView(HWND ParentWindow) : Customdirview::TCustomDirView(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE System::ResourceString _SUnixDefaultRootName;
#define Customunixdirview_SUnixDefaultRootName System::LoadResourceString(&Customunixdirview::_SUnixDefaultRootName)

}	/* namespace Customunixdirview */
using namespace Customunixdirview;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// CustomunixdirviewHPP
