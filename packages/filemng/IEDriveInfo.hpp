// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Iedriveinfo.pas' rev: 21.00

#ifndef IedriveinfoHPP
#define IedriveinfoHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Registry.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Comctrls.hpp>	// Pascal unit
#include <Shellapi.hpp>	// Pascal unit
#include <Shlobj.hpp>	// Pascal unit
#include <Commctrl.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Baseutils.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Iedriveinfo
{
//-- type declarations -------------------------------------------------------
typedef System::WideChar TDrive;

struct TDriveInfoRec;
typedef TDriveInfoRec *PDriveInfoRec;

struct TDriveInfoRec
{
	
public:
	_ITEMIDLIST *PIDL;
	bool Init;
	bool Valid;
	bool DriveReady;
	int DriveType;
	System::UnicodeString DisplayName;
	System::UnicodeString Prettyname;
	System::UnicodeString LongPrettyName;
	unsigned DriveSerial;
	__int64 Size;
	int ImageIndex;
	System::UnicodeString FileSystemName;
	unsigned MaxFileNameLength;
	unsigned FileSystemFlags;
};


typedef ShortInt TSpecialFolder;

struct TSpecialFolderRec;
typedef TSpecialFolderRec *PSpecialFolderRec;

struct TSpecialFolderRec
{
	
public:
	bool Valid;
	System::UnicodeString Location;
	System::UnicodeString DisplayName;
	int ImageIndex;
	_ITEMIDLIST *PIDL;
};


class DELPHICLASS TDriveInfo;
class PASCALIMPLEMENTATION TDriveInfo : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	typedef StaticArray<TDriveInfoRec, 26> _TDriveInfo__1;
	
	typedef StaticArray<TSpecialFolderRec, 28> _TDriveInfo__2;
	
	
public:
	PDriveInfoRec operator[](System::WideChar Drive) { return Data[Drive]; }
	
private:
	_TDriveInfo__1 FData;
	unsigned FNoDrives;
	_di_IShellFolder FDesktop;
	_TDriveInfo__2 FFolders;
	PDriveInfoRec __fastcall GetData(System::WideChar Drive);
	PSpecialFolderRec __fastcall GetFolder(TSpecialFolder Folder);
	
public:
	__property PDriveInfoRec Data[System::WideChar Drive] = {read=GetData/*, default*/};
	__property PSpecialFolderRec SpecialFolder[TSpecialFolder Folder] = {read=GetFolder};
	int __fastcall GetImageIndex(System::WideChar Drive);
	System::UnicodeString __fastcall GetDisplayName(System::WideChar Drive);
	System::UnicodeString __fastcall GetPrettyName(System::WideChar Drive);
	System::UnicodeString __fastcall GetLongPrettyName(System::WideChar Drive);
	bool __fastcall ReadDriveStatus(System::WideChar Drive, int Flags);
	__fastcall TDriveInfo(void);
	__fastcall virtual ~TDriveInfo(void);
	void __fastcall Load(void);
};


//-- var, const, procedure ---------------------------------------------------
static const ShortInt dsValid = 0x0;
static const ShortInt dsImageIndex = 0x1;
static const ShortInt dsSize = 0x2;
static const ShortInt dsDisplayName = 0x4;
static const ShortInt dsAll = 0x7;
static const WideChar FirstDrive = (WideChar)(0x41);
static const WideChar FirstFixedDrive = (WideChar)(0x43);
static const WideChar LastDrive = (WideChar)(0x5a);
static const ShortInt FirstSpecialFolder = 0x0;
static const ShortInt LastSpecialFolder = 0x1b;
extern PACKAGE TDriveInfo* DriveInfo;
extern PACKAGE System::ResourceString _ErrorInvalidDrive;
#define Iedriveinfo_ErrorInvalidDrive System::LoadResourceString(&Iedriveinfo::_ErrorInvalidDrive)
extern PACKAGE System::UnicodeString __fastcall GetShellFileName(const System::UnicodeString Name)/* overload */;
extern PACKAGE System::UnicodeString __fastcall GetShellFileName(Shlobj::PItemIDList PIDL)/* overload */;
extern PACKAGE System::UnicodeString __fastcall GetNetWorkName(System::WideChar Drive);
extern PACKAGE bool __fastcall GetNetWorkConnected(System::WideChar Drive);

}	/* namespace Iedriveinfo */
using namespace Iedriveinfo;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// IedriveinfoHPP
