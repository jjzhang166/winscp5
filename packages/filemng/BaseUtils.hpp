// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Baseutils.pas' rev: 21.00

#ifndef BaseutilsHPP
#define BaseutilsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Shlobj.hpp>	// Pascal unit
#include <Pidl.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Baseutils
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TDateTimePrecision { tpNone, tpDay, tpMinute, tpSecond, tpMillisecond };
#pragma option pop

//-- var, const, procedure ---------------------------------------------------
extern PACKAGE System::ResourceString _SNoValidPath;
#define Baseutils_SNoValidPath System::LoadResourceString(&Baseutils::_SNoValidPath)
extern PACKAGE System::ResourceString _SUcpPathsNotSupported;
#define Baseutils_SUcpPathsNotSupported System::LoadResourceString(&Baseutils::_SUcpPathsNotSupported)
extern PACKAGE System::UnicodeString __fastcall AnyValidPath(void);
extern PACKAGE bool __fastcall IsUncPath(System::UnicodeString Path);
extern PACKAGE void __fastcall StrTranslate(System::UnicodeString &Str, System::UnicodeString Code);
extern PACKAGE bool __fastcall StrContains(System::UnicodeString Str1, System::UnicodeString Str2);
extern PACKAGE bool __fastcall FileOrDirExists(System::UnicodeString FileName);
extern PACKAGE bool __fastcall CheckFileExists(System::UnicodeString FileName);
extern PACKAGE bool __fastcall DirExists(System::UnicodeString Dir, int &Attrs)/* overload */;
extern PACKAGE bool __fastcall DirExists(System::UnicodeString Dir)/* overload */;
extern PACKAGE System::UnicodeString __fastcall ExtractFileNameOnly(System::UnicodeString Name);
extern PACKAGE System::UnicodeString __fastcall FormatBytes(__int64 Bytes, bool UseOrders = true, bool UseUnitsForBytes = true);
extern PACKAGE void __fastcall FreePIDL(Shlobj::PItemIDList &PIDL);
extern PACKAGE __int64 __fastcall DiskSize(System::Byte Drive);
extern PACKAGE void __fastcall ReduceDateTimePrecision(System::TDateTime &DateTime, TDateTimePrecision Precision);
extern PACKAGE bool __fastcall SpecialFolderLocation(int Folder, System::UnicodeString &Path, Shlobj::PItemIDList &PIDL)/* overload */;
extern PACKAGE bool __fastcall SpecialFolderLocation(int Folder, System::UnicodeString &Path)/* overload */;
extern PACKAGE Controls::TImageList* __fastcall ShellImageList(Classes::TComponent* Owner, unsigned Flags);
extern PACKAGE System::UnicodeString __fastcall FormatLastOSError(System::UnicodeString Message);

}	/* namespace Baseutils */
using namespace Baseutils;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// BaseutilsHPP
