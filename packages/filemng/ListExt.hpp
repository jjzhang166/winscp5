// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Listext.pas' rev: 21.00

#ifndef ListextHPP
#define ListextHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Listext
{
//-- type declarations -------------------------------------------------------
typedef int IntType;

typedef void __fastcall (__closure *TLxDeleteEvent)(System::TObject* Sender, void * &P, int Size);

class DELPHICLASS TListExt;
class PASCALIMPLEMENTATION TListExt : public System::TInterfacedObject
{
	typedef System::TInterfacedObject inherited;
	
private:
	typedef DynamicArray<void *> _TListExt__1;
	
	
public:
	void * operator[](int i) { return data[i]; }
	
private:
	int FCount;
	_TListExt__1 FData;
	bool FSorted;
	TLxDeleteEvent fOnDelete;
	int FItemSize;
	int MaxCount;
	void * __fastcall GetItem(int I);
	void __fastcall FreeItem(int I);
	
public:
	__property void * data[int i] = {read=GetItem/*, default*/};
	__property bool Sorted = {read=FSorted, nodefault};
	__property int Count = {read=FCount, nodefault};
	__property int ItemSize = {read=FItemSize, nodefault};
	__fastcall TListExt(int ItemSize);
	HIDESBASE void __fastcall Free(void);
	void __fastcall Clear(void);
	void __fastcall Add(void * P);
	int __fastcall IndexOf(void * P);
	void __fastcall Sort(Classes::TListSortCompare Compare);
	int __fastcall Find(void * P, Classes::TListSortCompare Compare);
	int __fastcall FindSequential(void * P, Classes::TListSortCompare Compare);
	void * __fastcall First(void);
	void * __fastcall Last(void);
	void __fastcall Delete(int I);
	
__published:
	__property TLxDeleteEvent OnDelete = {read=fOnDelete, write=fOnDelete};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TListExt(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
static const Word InitSize = 0x1f4;
static const Word ExtendSize = 0x1f4;
static const ShortInt FLess = -1;
static const ShortInt FEqual = 0x0;
static const ShortInt FGreater = 0x1;

}	/* namespace Listext */
using namespace Listext;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// ListextHPP
