// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Dirviewcolproperties.pas' rev: 21.00

#ifndef DirviewcolpropertiesHPP
#define DirviewcolpropertiesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Comctrls.hpp>	// Pascal unit
#include <Ielistview.hpp>	// Pascal unit
#include <Listviewcolproperties.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dirviewcolproperties
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TCustomDirViewColProperties;
class PASCALIMPLEMENTATION TCustomDirViewColProperties : public Ielistview::TIEListViewColProperties
{
	typedef Ielistview::TIEListViewColProperties inherited;
	
protected:
	bool __fastcall GetSortByExtension(void);
	void __fastcall SetSortByExtension(bool Value);
	virtual System::UnicodeString __fastcall GetSortStr(void);
	virtual void __fastcall SetSortStr(System::UnicodeString Value);
	
public:
	__property bool SortByExtension = {read=GetSortByExtension, write=SetSortByExtension, default=0};
public:
	/* TIEListViewColProperties.Create */ inline __fastcall TCustomDirViewColProperties(Comctrls::TCustomListView* ListView, int ColCount) : Ielistview::TIEListViewColProperties(ListView, ColCount) { }
	
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TCustomDirViewColProperties(void) { }
	
};


#pragma option push -b-
enum TDirViewCol { dvName, dvSize, dvType, dvChanged, dvAttr, dvExt };
#pragma option pop

class DELPHICLASS TDirViewColProperties;
class PASCALIMPLEMENTATION TDirViewColProperties : public TCustomDirViewColProperties
{
	typedef TCustomDirViewColProperties inherited;
	
private:
	bool __fastcall StoreAlignment(int Index);
	bool __fastcall StoreCaption(int Index);
	bool __fastcall StoreWidth(int Index);
	TDirViewCol __fastcall GetDirOrder(int Index);
	TDirViewCol __fastcall GetSortDirColumn(void);
	void __fastcall SetDirOrder(int Index, TDirViewCol Value);
	void __fastcall SetSortDirColumn(TDirViewCol Value);
	
public:
	__fastcall TDirViewColProperties(Comctrls::TCustomListView* DirView);
	
__published:
	__property MaxWidth = {default=1000};
	__property MinWidth = {default=20};
	__property SortAscending = {default=1};
	__property SortByExtension = {default=0};
	__property TDirViewCol SortDirColumn = {read=GetSortDirColumn, write=SetSortDirColumn, default=0};
	__property System::UnicodeString NameCaption = {read=GetCaptions, write=SetCaptions, stored=StoreCaption, index=0};
	__property int NameWidth = {read=GetWidths, write=SetWidths, stored=StoreWidth, index=0, nodefault};
	__property bool NameVisible = {read=GetVisible, write=SetVisible, index=0, default=1};
	__property Classes::TAlignment NameAlignment = {read=GetAlignments, write=SetAlignments, stored=StoreAlignment, index=0, nodefault};
	__property System::UnicodeString SizeCaption = {read=GetCaptions, write=SetCaptions, stored=StoreCaption, index=1};
	__property int SizeWidth = {read=GetWidths, write=SetWidths, stored=StoreWidth, index=1, nodefault};
	__property bool SizeVisible = {read=GetVisible, write=SetVisible, index=1, default=1};
	__property Classes::TAlignment SizeAlignment = {read=GetAlignments, write=SetAlignments, stored=StoreAlignment, index=1, nodefault};
	__property System::UnicodeString TypeCaption = {read=GetCaptions, write=SetCaptions, stored=StoreCaption, index=2};
	__property int TypeWidth = {read=GetWidths, write=SetWidths, stored=StoreWidth, index=2, nodefault};
	__property bool TypeVisible = {read=GetVisible, write=SetVisible, index=2, default=1};
	__property Classes::TAlignment TypeAlignment = {read=GetAlignments, write=SetAlignments, stored=StoreAlignment, index=2, nodefault};
	__property System::UnicodeString ChangedCaption = {read=GetCaptions, write=SetCaptions, stored=StoreCaption, index=3};
	__property int ChangedWidth = {read=GetWidths, write=SetWidths, stored=StoreWidth, index=3, nodefault};
	__property bool ChangedVisible = {read=GetVisible, write=SetVisible, index=3, default=1};
	__property Classes::TAlignment ChangedAlignment = {read=GetAlignments, write=SetAlignments, stored=StoreAlignment, index=3, nodefault};
	__property System::UnicodeString AttrCaption = {read=GetCaptions, write=SetCaptions, stored=StoreCaption, index=4};
	__property int AttrWidth = {read=GetWidths, write=SetWidths, stored=StoreWidth, index=4, nodefault};
	__property bool AttrVisible = {read=GetVisible, write=SetVisible, index=4, default=1};
	__property Classes::TAlignment AttrAlignment = {read=GetAlignments, write=SetAlignments, stored=StoreAlignment, index=4, nodefault};
	__property System::UnicodeString ExtCaption = {read=GetCaptions, write=SetCaptions, stored=StoreCaption, index=5};
	__property int ExtWidth = {read=GetWidths, write=SetWidths, stored=StoreWidth, index=5, nodefault};
	__property bool ExtVisible = {read=GetVisible, write=SetVisible, index=5, default=1};
	__property Classes::TAlignment ExtAlignment = {read=GetAlignments, write=SetAlignments, stored=StoreAlignment, index=5, nodefault};
	__property TDirViewCol Column1 = {read=GetDirOrder, write=SetDirOrder, index=0, default=0};
	__property TDirViewCol Column2 = {read=GetDirOrder, write=SetDirOrder, index=1, default=1};
	__property TDirViewCol Column3 = {read=GetDirOrder, write=SetDirOrder, index=2, default=2};
	__property TDirViewCol Column4 = {read=GetDirOrder, write=SetDirOrder, index=3, default=3};
	__property TDirViewCol Column5 = {read=GetDirOrder, write=SetDirOrder, index=4, default=4};
	__property TDirViewCol Column6 = {read=GetDirOrder, write=SetDirOrder, index=5, default=5};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TDirViewColProperties(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE System::ResourceString _SDirViewNameCol;
#define Dirviewcolproperties_SDirViewNameCol System::LoadResourceString(&Dirviewcolproperties::_SDirViewNameCol)
extern PACKAGE System::ResourceString _SDirViewSizeCol;
#define Dirviewcolproperties_SDirViewSizeCol System::LoadResourceString(&Dirviewcolproperties::_SDirViewSizeCol)
extern PACKAGE System::ResourceString _SDirViewTypeCol;
#define Dirviewcolproperties_SDirViewTypeCol System::LoadResourceString(&Dirviewcolproperties::_SDirViewTypeCol)
extern PACKAGE System::ResourceString _SDirViewChangedCol;
#define Dirviewcolproperties_SDirViewChangedCol System::LoadResourceString(&Dirviewcolproperties::_SDirViewChangedCol)
extern PACKAGE System::ResourceString _SDirViewAttrCol;
#define Dirviewcolproperties_SDirViewAttrCol System::LoadResourceString(&Dirviewcolproperties::_SDirViewAttrCol)
extern PACKAGE System::ResourceString _SDirViewExtCol;
#define Dirviewcolproperties_SDirViewExtCol System::LoadResourceString(&Dirviewcolproperties::_SDirViewExtCol)
static const ShortInt DirViewColumns = 0x6;
extern PACKAGE StaticArray<void *, 6> DefaultDirViewCaptions;
extern PACKAGE StaticArray<int, 6> DefaultDirViewWidths;
extern PACKAGE StaticArray<Classes::TAlignment, 6> DefaultDirViewAlignments;
extern PACKAGE StaticArray<bool, 6> DefaultDirViewVisible;

}	/* namespace Dirviewcolproperties */
using namespace Dirviewcolproperties;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DirviewcolpropertiesHPP
