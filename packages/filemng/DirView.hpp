// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Dirview.pas' rev: 21.00

#ifndef DirviewHPP
#define DirviewHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Shlobj.hpp>	// Pascal unit
#include <Comctrls.hpp>	// Pascal unit
#include <Compthread.hpp>	// Pascal unit
#include <Customdirview.hpp>	// Pascal unit
#include <Listext.hpp>	// Pascal unit
#include <Extctrls.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit
#include <Fileoperator.hpp>	// Pascal unit
#include <Discmon.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Dirviewcolproperties.hpp>	// Pascal unit
#include <Dragdrop.hpp>	// Pascal unit
#include <Messages.hpp>	// Pascal unit
#include <Listviewcolproperties.hpp>	// Pascal unit
#include <Commctrl.hpp>	// Pascal unit
#include <Dragdropfilesex.hpp>	// Pascal unit
#include <Filectrl.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Baseutils.hpp>	// Pascal unit
#include <Ielistview.hpp>	// Pascal unit
#include <Nortonlikelistview.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit
#include <Pathlabel.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dirview
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TVolumeDisplayStyle { doPrettyName, doDisplayName, doLongPrettyName };
#pragma option pop

class DELPHICLASS EIUThread;
class PASCALIMPLEMENTATION EIUThread : public Sysutils::Exception
{
	typedef Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall EIUThread(const System::UnicodeString Msg) : Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall EIUThread(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size) : Sysutils::Exception(Msg, Args, Args_Size) { }
	/* Exception.CreateRes */ inline __fastcall EIUThread(int Ident)/* overload */ : Sysutils::Exception(Ident) { }
	/* Exception.CreateResFmt */ inline __fastcall EIUThread(int Ident, System::TVarRec const *Args, const int Args_Size)/* overload */ : Sysutils::Exception(Ident, Args, Args_Size) { }
	/* Exception.CreateHelp */ inline __fastcall EIUThread(const System::UnicodeString Msg, int AHelpContext) : Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall EIUThread(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size, int AHelpContext) : Sysutils::Exception(Msg, Args, Args_Size, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall EIUThread(int Ident, int AHelpContext)/* overload */ : Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall EIUThread(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size, int AHelpContext)/* overload */ : Sysutils::Exception(ResStringRec, Args, Args_Size, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~EIUThread(void) { }
	
};


class DELPHICLASS EDragDrop;
class PASCALIMPLEMENTATION EDragDrop : public Sysutils::Exception
{
	typedef Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall EDragDrop(const System::UnicodeString Msg) : Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall EDragDrop(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size) : Sysutils::Exception(Msg, Args, Args_Size) { }
	/* Exception.CreateRes */ inline __fastcall EDragDrop(int Ident)/* overload */ : Sysutils::Exception(Ident) { }
	/* Exception.CreateResFmt */ inline __fastcall EDragDrop(int Ident, System::TVarRec const *Args, const int Args_Size)/* overload */ : Sysutils::Exception(Ident, Args, Args_Size) { }
	/* Exception.CreateHelp */ inline __fastcall EDragDrop(const System::UnicodeString Msg, int AHelpContext) : Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall EDragDrop(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size, int AHelpContext) : Sysutils::Exception(Msg, Args, Args_Size, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall EDragDrop(int Ident, int AHelpContext)/* overload */ : Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall EDragDrop(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size, int AHelpContext)/* overload */ : Sysutils::Exception(ResStringRec, Args, Args_Size, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~EDragDrop(void) { }
	
};


class DELPHICLASS EInvalidFileName;
class PASCALIMPLEMENTATION EInvalidFileName : public Sysutils::Exception
{
	typedef Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall EInvalidFileName(const System::UnicodeString Msg) : Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall EInvalidFileName(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size) : Sysutils::Exception(Msg, Args, Args_Size) { }
	/* Exception.CreateRes */ inline __fastcall EInvalidFileName(int Ident)/* overload */ : Sysutils::Exception(Ident) { }
	/* Exception.CreateResFmt */ inline __fastcall EInvalidFileName(int Ident, System::TVarRec const *Args, const int Args_Size)/* overload */ : Sysutils::Exception(Ident, Args, Args_Size) { }
	/* Exception.CreateHelp */ inline __fastcall EInvalidFileName(const System::UnicodeString Msg, int AHelpContext) : Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall EInvalidFileName(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size, int AHelpContext) : Sysutils::Exception(Msg, Args, Args_Size, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall EInvalidFileName(int Ident, int AHelpContext)/* overload */ : Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall EInvalidFileName(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size, int AHelpContext)/* overload */ : Sysutils::Exception(ResStringRec, Args, Args_Size, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~EInvalidFileName(void) { }
	
};


class DELPHICLASS ERenameFileFailed;
class PASCALIMPLEMENTATION ERenameFileFailed : public Sysutils::Exception
{
	typedef Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall ERenameFileFailed(const System::UnicodeString Msg) : Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall ERenameFileFailed(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size) : Sysutils::Exception(Msg, Args, Args_Size) { }
	/* Exception.CreateRes */ inline __fastcall ERenameFileFailed(int Ident)/* overload */ : Sysutils::Exception(Ident) { }
	/* Exception.CreateResFmt */ inline __fastcall ERenameFileFailed(int Ident, System::TVarRec const *Args, const int Args_Size)/* overload */ : Sysutils::Exception(Ident, Args, Args_Size) { }
	/* Exception.CreateHelp */ inline __fastcall ERenameFileFailed(const System::UnicodeString Msg, int AHelpContext) : Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall ERenameFileFailed(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size, int AHelpContext) : Sysutils::Exception(Msg, Args, Args_Size, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ERenameFileFailed(int Ident, int AHelpContext)/* overload */ : Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ERenameFileFailed(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size, int AHelpContext)/* overload */ : Sysutils::Exception(ResStringRec, Args, Args_Size, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~ERenameFileFailed(void) { }
	
};


typedef WideChar TDriveLetter;

#pragma option push -b-
enum TClipboardOperation { cboNone, cboCut, cboCopy };
#pragma option pop

#pragma option push -b-
enum TFileNameDisplay { fndStored, fndCap, fndNoCap, fndNice };
#pragma option pop

struct TFileRec;
typedef TFileRec *PFileRec;

struct TFileRec
{
	
public:
	bool Empty;
	bool IconEmpty;
	bool IsDirectory;
	bool IsRecycleBin;
	bool IsParentDir;
	System::UnicodeString FileName;
	System::UnicodeString Displayname;
	System::UnicodeString FileExt;
	System::UnicodeString TypeName;
	int ImageIndex;
	__int64 Size;
	unsigned Attr;
	_FILETIME FileTime;
	_ITEMIDLIST *PIDL;
};


struct TInfoCache;
typedef TInfoCache *PInfoCache;

struct TInfoCache
{
	
public:
	System::UnicodeString FileExt;
	System::UnicodeString TypeName;
	int ImageIndex;
};


typedef void __fastcall (__closure *TDirViewAddFileEvent)(System::TObject* Sender, Sysutils::TSearchRec &SearchRec, bool &AddFile);

typedef void __fastcall (__closure *TDirViewFileSizeChanged)(System::TObject* Sender, Comctrls::TListItem* Item);

typedef void __fastcall (__closure *TDirViewFileIconForName)(System::TObject* Sender, Comctrls::TListItem* Item, System::UnicodeString &FileName);

class DELPHICLASS TSubDirScanner;
class DELPHICLASS TDirView;
class PASCALIMPLEMENTATION TSubDirScanner : public Compthread::TCompThread
{
	typedef Compthread::TCompThread inherited;
	
private:
	TDirView* FOwner;
	System::UnicodeString FStartPath;
	System::UnicodeString FDirName;
	__int64 FTotalSize;
	void __fastcall ThreadTerminated(System::TObject* Sender);
	
protected:
	__fastcall TSubDirScanner(TDirView* Owner, Comctrls::TListItem* Item);
	void __fastcall DoUpdateItem(void);
	virtual void __fastcall Execute(void);
public:
	/* TCompThread.Destroy */ inline __fastcall virtual ~TSubDirScanner(void) { }
	
};


class DELPHICLASS TIconUpdateThread;
class PASCALIMPLEMENTATION TIconUpdateThread : public Compthread::TCompThread
{
	typedef Compthread::TCompThread inherited;
	
private:
	TDirView* FOwner;
	int FIndex;
	int FMaxIndex;
	bool FNewIcons;
	int FSyncIcon;
	int CurrentIndex;
	System::UnicodeString CurrentFilePath;
	TFileRec CurrentItemData;
	bool InvalidItem;
	void __fastcall SetIndex(int Value);
	void __fastcall SetMaxIndex(int Value);
	
protected:
	__fastcall TIconUpdateThread(TDirView* Owner);
	void __fastcall DoFetchData(void);
	void __fastcall DoUpdateIcon(void);
	virtual void __fastcall Execute(void);
	__property int Index = {read=FIndex, write=SetIndex, nodefault};
	__property int MaxIndex = {read=FMaxIndex, write=SetMaxIndex, nodefault};
	
public:
	virtual void __fastcall Terminate(void);
public:
	/* TCompThread.Destroy */ inline __fastcall virtual ~TIconUpdateThread(void) { }
	
};


class PASCALIMPLEMENTATION TDirView : public Customdirview::TCustomDirView
{
	typedef Customdirview::TCustomDirView inherited;
	
private:
	typedef StaticArray<System::UnicodeString, 26> _TDirView__1;
	
	
private:
	bool FConfirmDelete;
	bool FConfirmOverwrite;
	bool FUseIconCache;
	Listext::TListExt* FInfoCacheList;
	System::TObject* FDriveView;
	Extctrls::TTimer* FChangeTimer;
	unsigned FChangeInterval;
	bool FUseIconUpdateThread;
	bool FIUThreadFinished;
	int FDriveType;
	System::UnicodeString FAttrSpace;
	System::UnicodeString FNoCheckDrives;
	bool FSortAfterUpdate;
	Graphics::TColor FCompressedColor;
	TFileNameDisplay FFileNameDisplay;
	_di_IShellFolder FParentFolder;
	_di_IShellFolder FDesktopFolder;
	bool FDirOK;
	System::UnicodeString FPath;
	bool FDrawLinkOverlay;
	bool SelectNewFiles;
	bool FSelfDropDuplicates;
	int FHiddenCount;
	int FFilteredCount;
	Customdirview::TSelAttr FSelArchive;
	Customdirview::TSelAttr FSelHidden;
	Customdirview::TSelAttr FSelSysFile;
	Customdirview::TSelAttr FSelReadOnly;
	__int64 FSelFileSizeFrom;
	__int64 FSelFileSizeTo;
	System::Word FSelFileDateFrom;
	System::Word FSelFileDateTo;
	System::Word FSelFileTimeFrom;
	System::Word FSelFileTimeTo;
	Fileoperator::TFileOperator* FFileOperator;
	TIconUpdateThread* FIconUpdateThread;
	Discmon::TDiscMonitor* FDiscMonitor;
	System::UnicodeString FHomeDirectory;
	Classes::TList* FSubDirScanner;
	TDirViewAddFileEvent FOnAddFile;
	TDirViewFileSizeChanged FOnFileSizeChanged;
	TDirViewFileIconForName FOnFileIconForName;
	Classes::TNotifyEvent FOnChangeDetected;
	Classes::TNotifyEvent FOnChangeInvalid;
	_di_IShellFolder iRecycleFolder;
	_ITEMIDLIST *PIDLRecycle;
	_TDirView__1 FLastPath;
	Dirviewcolproperties::TDirViewColProperties* __fastcall GetDirColProperties(void);
	System::UnicodeString __fastcall GetHomeDirectory(void);
	void __fastcall SignalFileDelete(System::TObject* Sender, Classes::TStringList* Files);
	void __fastcall PerformDragDropFileOperation(System::UnicodeString TargetPath, int dwEffect, bool RenameOnCollision);
	void __fastcall SetDirColProperties(Dirviewcolproperties::TDirViewColProperties* Value);
	
protected:
	virtual Listviewcolproperties::TCustomListViewColProperties* __fastcall NewColProperties(void);
	virtual bool __fastcall SortAscendingByDefault(int Index);
	virtual void __fastcall SetShowSubDirSize(bool Value);
	virtual void __fastcall Notification(Classes::TComponent* AComponent, Classes::TOperation Operation);
	DYNAMIC void __fastcall Delete(Comctrls::TListItem* Item);
	void __fastcall DDError(Customdirview::TDDError ErrorNo);
	virtual bool __fastcall GetCanUndoCopyMove(void);
	_di_IShellFolder __fastcall GetShellFolder(System::UnicodeString Dir);
	virtual bool __fastcall GetDirOK(void);
	virtual void __fastcall GetDisplayInfo(Comctrls::TListItem* ListItem, tagLVITEMW &DispInfo);
	virtual void __fastcall DDDragDetect(int grfKeyState, const Types::TPoint &DetectStart, const Types::TPoint &Point, Dragdrop::TDragDetectStatus DragStatus);
	virtual void __fastcall DDMenuDone(System::TObject* Sender, HMENU AMenu);
	virtual void __fastcall DDDropHandlerSucceeded(System::TObject* Sender, int grfKeyState, const Types::TPoint &Point, int dwEffect);
	virtual void __fastcall DDChooseEffect(int grfKeyState, int &dwEffect);
	virtual System::UnicodeString __fastcall GetPathName(void);
	virtual void __fastcall SetChangeInterval(unsigned Value);
	virtual void __fastcall LoadFromRecycleBin(System::UnicodeString Dir);
	virtual void __fastcall SetLoadEnabled(bool Value);
	virtual System::UnicodeString __fastcall GetPath(void);
	virtual void __fastcall SetPath(System::UnicodeString Value);
	virtual void __fastcall PathChanged(void);
	virtual void __fastcall SetItemImageIndex(Comctrls::TListItem* Item, int Index);
	void __fastcall SetCompressedColor(Graphics::TColor Value);
	void __fastcall ChangeDetected(System::TObject* Sender, const System::UnicodeString Directory, bool &SubdirsChanged);
	void __fastcall ChangeInvalid(System::TObject* Sender, const System::UnicodeString Directory, const System::UnicodeString ErrorStr);
	void __fastcall TimerOnTimer(System::TObject* Sender);
	void __fastcall ResetItemImage(int Index);
	void __fastcall SetAttrSpace(System::UnicodeString Value);
	void __fastcall SetNoCheckDrives(System::UnicodeString Value);
	virtual void __fastcall SetWatchForChanges(bool Value);
	void __fastcall AddParentDirItem(void);
	virtual void __fastcall AddToDragFileList(Dragdropfilesex::TFileList* FileList, Comctrls::TListItem* Item);
	virtual void __fastcall SetFileNameDisplay(TFileNameDisplay Value);
	virtual void __fastcall DisplayContextMenu(const Types::TPoint &Where);
	virtual bool __fastcall DragCompleteFileList(void);
	virtual void __fastcall ExecuteFile(Comctrls::TListItem* Item);
	virtual bool __fastcall GetIsRoot(void);
	virtual void __fastcall InternalEdit(const tagLVITEMW &HItem);
	virtual Graphics::TColor __fastcall ItemColor(Comctrls::TListItem* Item);
	virtual System::UnicodeString __fastcall ItemDisplayName(System::UnicodeString FileName);
	System::UnicodeString __fastcall ItemFileExt(Comctrls::TListItem* Item);
	System::UnicodeString __fastcall ItemFileNameOnly(Comctrls::TListItem* Item);
	virtual int __fastcall ItemImageIndex(Comctrls::TListItem* Item, bool Cache);
	virtual bool __fastcall ItemIsFile(Comctrls::TListItem* Item);
	virtual bool __fastcall ItemIsRecycleBin(Comctrls::TListItem* Item);
	virtual bool __fastcall ItemMatchesFilter(Comctrls::TListItem* Item, const Customdirview::TFileFilter &Filter);
	virtual System::Word __fastcall ItemOverlayIndexes(Comctrls::TListItem* Item);
	virtual void __fastcall LoadFiles(void);
	virtual System::UnicodeString __fastcall MinimizePath(System::UnicodeString Path, int Len);
	virtual void __fastcall PerformItemDragDropOperation(Comctrls::TListItem* Item, int Effect);
	virtual void __fastcall SortItems(void);
	void __fastcall StartFileDeleteThread(void);
	virtual void __fastcall SetShowHiddenFiles(bool Value);
	HIDESBASE MESSAGE void __fastcall WMDestroy(Messages::TWMNoParams &Msg);
	virtual int __fastcall SecondaryColumnHeader(int Index, bool &AliasOnly);
	virtual int __fastcall HiddenCount(void);
	virtual int __fastcall FilteredCount(void);
	
public:
	__property int DriveType = {read=FDriveType, nodefault};
	__property System::TObject* DriveView = {read=FDriveView, write=FDriveView};
	__property Items = {stored=false};
	__property Columns = {stored=false};
	__property _di_IShellFolder ParentFolder = {read=FParentFolder};
	__property bool CanUndoCopyMove = {read=GetCanUndoCopyMove, nodefault};
	__property Fileoperator::TFileOperator* DDFileOperator = {read=FFileOperator};
	DYNAMIC bool __fastcall UndoCopyMove(void);
	DYNAMIC void __fastcall EmptyClipboard(void);
	DYNAMIC bool __fastcall CopyToClipBoard(void);
	DYNAMIC bool __fastcall CutToClipBoard(void);
	virtual bool __fastcall PasteFromClipBoard(System::UnicodeString TargetPath = L"");
	DYNAMIC bool __fastcall DuplicateSelectedFiles(void);
	virtual void __fastcall DisplayPropertiesMenu(void);
	virtual void __fastcall ExecuteParentDirectory(void);
	virtual void __fastcall ExecuteRootDirectory(void);
	virtual bool __fastcall ItemIsDirectory(Comctrls::TListItem* Item);
	virtual System::UnicodeString __fastcall ItemFullFileName(Comctrls::TListItem* Item);
	virtual bool __fastcall ItemIsParentDirectory(Comctrls::TListItem* Item);
	virtual System::UnicodeString __fastcall ItemFileName(Comctrls::TListItem* Item);
	virtual __int64 __fastcall ItemFileSize(Comctrls::TListItem* Item);
	virtual System::TDateTime __fastcall ItemFileTime(Comctrls::TListItem* Item, Baseutils::TDateTimePrecision &Precision);
	void __fastcall StartWatchThread(void);
	void __fastcall StopWatchThread(void);
	bool __fastcall WatchThreadActive(void);
	void __fastcall StartIconUpdateThread(void);
	void __fastcall StopIconUpdateThread(void);
	void __fastcall StartSubDirScanner(void);
	void __fastcall StopSubDirScanner(void);
	void __fastcall TerminateThreads(void);
	void __fastcall Syncronize(void);
	void __fastcall ClearIconCache(void);
	DYNAMIC Comctrls::TListItem* __fastcall CreateFile(System::UnicodeString NewName);
	virtual void __fastcall CreateDirectory(System::UnicodeString DirName);
	DYNAMIC bool __fastcall DeleteSelectedFiles(bool AllowUndo);
	void __fastcall ValidateFile(Comctrls::TListItem* Item)/* overload */;
	void __fastcall ValidateFile(Sysutils::TFileName FileName)/* overload */;
	DYNAMIC void __fastcall ValidateSelectedFiles(void);
	HIDESBASE Comctrls::TListItem* __fastcall AddItem(const Sysutils::TSearchRec &SRec);
	void __fastcall GetDisplayData(Comctrls::TListItem* Item, bool FetchIcon);
	PFileRec __fastcall GetFileRec(int Index);
	virtual void __fastcall Load(void);
	virtual void __fastcall Reload(bool CacheIcons);
	void __fastcall Reload2(void);
	virtual System::UnicodeString __fastcall FormatFileTime(const _FILETIME &FileTime);
	virtual System::UnicodeString __fastcall GetAttrString(int Attr);
	void __fastcall FetchAllDisplayData(void);
	__fastcall virtual TDirView(Classes::TComponent* AOwner);
	__fastcall virtual ~TDirView(void);
	virtual void __fastcall ExecuteHomeDirectory(void);
	virtual void __fastcall ReloadDirectory(void);
	void __fastcall ExecuteDrive(TDriveLetter Drive);
	__property System::UnicodeString HomeDirectory = {read=GetHomeDirectory, write=FHomeDirectory};
	__property Customdirview::TSelAttr SelArchive = {read=FSelArchive, write=FSelArchive, default=0};
	__property Customdirview::TSelAttr SelHidden = {read=FSelHidden, write=FSelHidden, default=0};
	__property Customdirview::TSelAttr SelSysFile = {read=FSelSysFile, write=FSelSysFile, default=0};
	__property Customdirview::TSelAttr SelReadOnly = {read=FSelReadOnly, write=FSelReadOnly, default=0};
	__property __int64 SelFileSizeFrom = {read=FSelFileSizeFrom, write=FSelFileSizeFrom};
	__property __int64 SelFileSizeTo = {read=FSelFileSizeTo, write=FSelFileSizeTo, default=0};
	__property System::Word SelFileDateFrom = {read=FSelFileDateFrom, write=FSelFileDateFrom, default=33};
	__property System::Word SelFileDateTo = {read=FSelFileDateTo, write=FSelFileDateTo, default=61343};
	__property System::Word SelFileTimeFrom = {read=FSelFileTimeFrom, write=FSelFileTimeFrom, nodefault};
	__property System::Word SelFileTimeTo = {read=FSelFileTimeTo, write=FSelFileTimeTo, default=49152};
	
__published:
	__property Dirviewcolproperties::TDirViewColProperties* DirColProperties = {read=GetDirColProperties, write=SetDirColProperties};
	__property PathLabel;
	__property OnUpdateStatusBar;
	__property OnGetSelectFilter;
	__property HeaderImages;
	__property LoadAnimation = {default=1};
	__property DimmHiddenFiles = {default=1};
	__property ShowDirectories = {default=1};
	__property ShowHiddenFiles = {default=1};
	__property ShowSubDirSize = {default=0};
	__property SingleClickToExec = {default=0};
	__property WantUseDragImages = {default=1};
	__property TargetPopupMenu = {default=1};
	__property AddParentDir = {default=0};
	__property OnSelectItem;
	__property OnStartLoading;
	__property OnLoaded;
	__property OnDDDragEnter;
	__property OnDDDragLeave;
	__property OnDDDragOver;
	__property OnDDDrop;
	__property OnDDQueryContinueDrag;
	__property OnDDGiveFeedback;
	__property OnDDDragDetect;
	__property OnDDCreateDragFileList;
	__property OnDDEnd;
	__property OnDDCreateDataObject;
	__property OnDDTargetHasDropHandler;
	__property DDLinkOnExeDrag = {default=1};
	__property OnDDProcessDropped;
	__property OnDDError;
	__property OnDDExecuted;
	__property OnDDFileOperation;
	__property OnDDFileOperationExecuted;
	__property OnDDMenuPopup;
	__property OnExecFile;
	__property OnMatchMask;
	__property OnGetOverlay;
	__property Graphics::TColor CompressedColor = {read=FCompressedColor, write=SetCompressedColor, default=16711680};
	__property bool ConfirmDelete = {read=FConfirmDelete, write=FConfirmDelete, default=1};
	__property bool ConfirmOverwrite = {read=FConfirmOverwrite, write=FConfirmOverwrite, default=1};
	__property bool SortAfterUpdate = {read=FSortAfterUpdate, write=FSortAfterUpdate, default=1};
	__property unsigned ChangeInterval = {read=FChangeInterval, write=SetChangeInterval, default=1000};
	__property bool UseIconUpdateThread = {read=FUseIconUpdateThread, write=FUseIconUpdateThread, default=0};
	__property bool UseIconCache = {read=FUseIconCache, write=FUseIconCache, default=0};
	__property TFileNameDisplay FileNameDisplay = {read=FFileNameDisplay, write=SetFileNameDisplay, default=0};
	__property System::UnicodeString AttrSpace = {read=FAttrSpace, write=SetAttrSpace};
	__property System::UnicodeString NoCheckDrives = {read=FNoCheckDrives, write=SetNoCheckDrives};
	__property WatchForChanges = {default=0};
	__property bool SelfDropDuplicates = {read=FSelfDropDuplicates, write=FSelfDropDuplicates, default=0};
	__property Classes::TNotifyEvent OnChangeDetected = {read=FOnChangeDetected, write=FOnChangeDetected};
	__property Classes::TNotifyEvent OnChangeInvalid = {read=FOnChangeInvalid, write=FOnChangeInvalid};
	__property TDirViewAddFileEvent OnAddFile = {read=FOnAddFile, write=FOnAddFile};
	__property TDirViewFileSizeChanged OnFileSizeChanged = {read=FOnFileSizeChanged, write=FOnFileSizeChanged};
	__property TDirViewFileIconForName OnFileIconForName = {read=FOnFileIconForName, write=FOnFileIconForName};
	__property UseSystemContextMenu = {default=1};
	__property OnContextPopup;
	__property OnBeginRename;
	__property OnEndRename;
	__property OnHistoryChange;
	__property OnHistoryGo;
	__property OnPathChange;
	__property ColumnClick = {default=1};
	__property MultiSelect = {default=1};
	__property ReadOnly = {default=0};
public:
	/* TWinControl.CreateParented */ inline __fastcall TDirView(HWND ParentWindow) : Customdirview::TCustomDirView(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE System::ResourceString _coFileOperatorTitle;
#define Dirview_coFileOperatorTitle System::LoadResourceString(&Dirview::_coFileOperatorTitle)
extern PACKAGE System::ResourceString _coInvalidDosChars;
#define Dirview_coInvalidDosChars System::LoadResourceString(&Dirview::_coInvalidDosChars)
extern PACKAGE System::ResourceString _Space;
#define Dirview_Space System::LoadResourceString(&Dirview::_Space)
static const ShortInt msThreadChangeDelay = 0xa;
static const ShortInt MaxWaitTimeOut = 0xa;
static const Word FileAttr = 0x1f7;
#define SpecialExtensions L"EXE,LNK,ICO,ANI,CUR,PIF,JOB,CPL"
#define ExeExtension L"EXE"
static const ShortInt MinDate = 0x21;
static const Word MaxDate = 0xef9f;
static const ShortInt MinTime = 0x0;
static const Word MaxTime = 0xc000;
extern PACKAGE TClipboardOperation LastClipBoardOperation;
extern PACKAGE unsigned LastIOResult;
extern PACKAGE void __fastcall Register(void);
extern PACKAGE bool __fastcall MatchesFileExt(System::UnicodeString Ext, const System::UnicodeString FileExtList);

}	/* namespace Dirview */
using namespace Dirview;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DirviewHPP
