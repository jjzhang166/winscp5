// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Driveview.pas' rev: 21.00

#ifndef DriveviewHPP
#define DriveviewHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Messages.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Comobj.hpp>	// Pascal unit
#include <Dialogs.hpp>	// Pascal unit
#include <Comctrls.hpp>	// Pascal unit
#include <Shellapi.hpp>	// Pascal unit
#include <Commctrl.hpp>	// Pascal unit
#include <Extctrls.hpp>	// Pascal unit
#include <Activex.hpp>	// Pascal unit
#include <Shlobj.hpp>	// Pascal unit
#include <Dirview.hpp>	// Pascal unit
#include <Shelldialogs.hpp>	// Pascal unit
#include <Dragdrop.hpp>	// Pascal unit
#include <Dragdropfilesex.hpp>	// Pascal unit
#include <Filechanges.hpp>	// Pascal unit
#include <Fileoperator.hpp>	// Pascal unit
#include <Discmon.hpp>	// Pascal unit
#include <Iedriveinfo.hpp>	// Pascal unit
#include <Ielistview.hpp>	// Pascal unit
#include <Pidl.hpp>	// Pascal unit
#include <Baseutils.hpp>	// Pascal unit
#include <Listext.hpp>	// Pascal unit
#include <Customdirview.hpp>	// Pascal unit
#include <Customdriveview.hpp>	// Pascal unit
#include <Types.hpp>	// Pascal unit
#include <Menus.hpp>	// Pascal unit
#include <Imglist.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Driveview
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS ECreateShortCut;
class PASCALIMPLEMENTATION ECreateShortCut : public Sysutils::Exception
{
	typedef Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall ECreateShortCut(const System::UnicodeString Msg) : Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall ECreateShortCut(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size) : Sysutils::Exception(Msg, Args, Args_Size) { }
	/* Exception.CreateRes */ inline __fastcall ECreateShortCut(int Ident)/* overload */ : Sysutils::Exception(Ident) { }
	/* Exception.CreateResFmt */ inline __fastcall ECreateShortCut(int Ident, System::TVarRec const *Args, const int Args_Size)/* overload */ : Sysutils::Exception(Ident, Args, Args_Size) { }
	/* Exception.CreateHelp */ inline __fastcall ECreateShortCut(const System::UnicodeString Msg, int AHelpContext) : Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall ECreateShortCut(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size, int AHelpContext) : Sysutils::Exception(Msg, Args, Args_Size, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ECreateShortCut(int Ident, int AHelpContext)/* overload */ : Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ECreateShortCut(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size, int AHelpContext)/* overload */ : Sysutils::Exception(ResStringRec, Args, Args_Size, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~ECreateShortCut(void) { }
	
};


class DELPHICLASS EInvalidDirName;
class PASCALIMPLEMENTATION EInvalidDirName : public Sysutils::Exception
{
	typedef Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall EInvalidDirName(const System::UnicodeString Msg) : Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall EInvalidDirName(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size) : Sysutils::Exception(Msg, Args, Args_Size) { }
	/* Exception.CreateRes */ inline __fastcall EInvalidDirName(int Ident)/* overload */ : Sysutils::Exception(Ident) { }
	/* Exception.CreateResFmt */ inline __fastcall EInvalidDirName(int Ident, System::TVarRec const *Args, const int Args_Size)/* overload */ : Sysutils::Exception(Ident, Args, Args_Size) { }
	/* Exception.CreateHelp */ inline __fastcall EInvalidDirName(const System::UnicodeString Msg, int AHelpContext) : Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall EInvalidDirName(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size, int AHelpContext) : Sysutils::Exception(Msg, Args, Args_Size, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall EInvalidDirName(int Ident, int AHelpContext)/* overload */ : Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall EInvalidDirName(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size, int AHelpContext)/* overload */ : Sysutils::Exception(ResStringRec, Args, Args_Size, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~EInvalidDirName(void) { }
	
};


class DELPHICLASS EInvalidPath;
class PASCALIMPLEMENTATION EInvalidPath : public Sysutils::Exception
{
	typedef Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall EInvalidPath(const System::UnicodeString Msg) : Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall EInvalidPath(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size) : Sysutils::Exception(Msg, Args, Args_Size) { }
	/* Exception.CreateRes */ inline __fastcall EInvalidPath(int Ident)/* overload */ : Sysutils::Exception(Ident) { }
	/* Exception.CreateResFmt */ inline __fastcall EInvalidPath(int Ident, System::TVarRec const *Args, const int Args_Size)/* overload */ : Sysutils::Exception(Ident, Args, Args_Size) { }
	/* Exception.CreateHelp */ inline __fastcall EInvalidPath(const System::UnicodeString Msg, int AHelpContext) : Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall EInvalidPath(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size, int AHelpContext) : Sysutils::Exception(Msg, Args, Args_Size, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall EInvalidPath(int Ident, int AHelpContext)/* overload */ : Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall EInvalidPath(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size, int AHelpContext)/* overload */ : Sysutils::Exception(ResStringRec, Args, Args_Size, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~EInvalidPath(void) { }
	
};


class DELPHICLASS ENodeNotAssigned;
class PASCALIMPLEMENTATION ENodeNotAssigned : public Sysutils::Exception
{
	typedef Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall ENodeNotAssigned(const System::UnicodeString Msg) : Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall ENodeNotAssigned(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size) : Sysutils::Exception(Msg, Args, Args_Size) { }
	/* Exception.CreateRes */ inline __fastcall ENodeNotAssigned(int Ident)/* overload */ : Sysutils::Exception(Ident) { }
	/* Exception.CreateResFmt */ inline __fastcall ENodeNotAssigned(int Ident, System::TVarRec const *Args, const int Args_Size)/* overload */ : Sysutils::Exception(Ident, Args, Args_Size) { }
	/* Exception.CreateHelp */ inline __fastcall ENodeNotAssigned(const System::UnicodeString Msg, int AHelpContext) : Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall ENodeNotAssigned(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size, int AHelpContext) : Sysutils::Exception(Msg, Args, Args_Size, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ENodeNotAssigned(int Ident, int AHelpContext)/* overload */ : Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ENodeNotAssigned(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size, int AHelpContext)/* overload */ : Sysutils::Exception(ResStringRec, Args, Args_Size, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~ENodeNotAssigned(void) { }
	
};


struct TDriveStatus
{
	
public:
	bool Scanned;
	bool Verified;
	Comctrls::TTreeNode* RootNode;
	Discmon::TDiscMonitor* DiscMonitor;
	Extctrls::TTimer* ChangeTimer;
	System::UnicodeString DefaultDir;
};


struct TScanDirInfo
{
	
public:
	bool SearchNewDirs;
	Comctrls::TTreeNode* StartNode;
	int DriveType;
};


typedef TScanDirInfo *PScanDirInfo;

typedef void __fastcall (__closure *TDriveViewScanDirEvent)(System::TObject* Sender, Comctrls::TTreeNode* Node, bool &DoScanDir);

typedef void __fastcall (__closure *TDriveViewDiskChangeEvent)(System::TObject* Sender, System::WideChar Drive);

class DELPHICLASS TNodeData;
class PASCALIMPLEMENTATION TNodeData : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::UnicodeString FDirName;
	System::UnicodeString FShortName;
	int FAttr;
	bool FScanned;
	void *FData;
	bool FExpanded;
	unsigned FDirSize;
	bool FIsRecycleBin;
	bool FIconEmpty;
	
public:
	unsigned shAttr;
	_ITEMIDLIST *PIDL;
	_di_IShellFolder ShellFolder;
	__fastcall TNodeData(void);
	__fastcall virtual ~TNodeData(void);
	__property System::UnicodeString DirName = {read=FDirName, write=FDirName};
	__property System::UnicodeString ShortName = {read=FShortName, write=FShortName};
	__property int Attr = {read=FAttr, write=FAttr, nodefault};
	__property bool Scanned = {read=FScanned, write=FScanned, nodefault};
	__property void * Data = {read=FData, write=FData};
	__property bool Expanded = {read=FExpanded, write=FExpanded, nodefault};
	__property unsigned DirSize = {read=FDirSize, write=FDirSize, nodefault};
	__property bool IsRecycleBin = {read=FIsRecycleBin, nodefault};
	__property bool IconEmpty = {read=FIconEmpty, write=FIconEmpty, nodefault};
};


class DELPHICLASS TDriveTreeNode;
class PASCALIMPLEMENTATION TDriveTreeNode : public Comctrls::TTreeNode
{
	typedef Comctrls::TTreeNode inherited;
	
__published:
	virtual void __fastcall Assign(Classes::TPersistent* Source);
public:
	/* TTreeNode.Create */ inline __fastcall virtual TDriveTreeNode(Comctrls::TTreeNodes* AOwner) : Comctrls::TTreeNode(AOwner) { }
	/* TTreeNode.Destroy */ inline __fastcall virtual ~TDriveTreeNode(void) { }
	
};


class DELPHICLASS TDriveView;
class PASCALIMPLEMENTATION TDriveView : public Customdriveview::TCustomDriveView
{
	typedef Customdriveview::TCustomDriveView inherited;
	
private:
	typedef StaticArray<TDriveStatus, 26> _TDriveView__1;
	
	
private:
	_TDriveView__1 DriveStatus;
	bool FConfirmDelete;
	bool FConfirmOverwrite;
	bool FWatchDirectory;
	System::UnicodeString FDirectory;
	bool FFullDriveScan;
	bool FShowDirSize;
	bool FShowVolLabel;
	Dirview::TVolumeDisplayStyle FVolDisplayStyle;
	bool FShowAnimation;
	bool FChangeFlag;
	System::UnicodeString FLastDir;
	bool FValidateFlag;
	bool FCreating;
	bool FForceRename;
	Comctrls::TTreeNode* FRenameNode;
	System::UnicodeString FLastRenameName;
	HWND FInternalWindowHandle;
	Comctrls::TTreeNode* FPrevSelected;
	_di_IShellFolder FDesktop;
	_di_IShellFolder FWorkPlace;
	Classes::TNotifyEvent FOnStartScan;
	Classes::TNotifyEvent FOnEndScan;
	TDriveViewScanDirEvent FOnScanDir;
	TDriveViewDiskChangeEvent FOnDiskChange;
	TDriveViewDiskChangeEvent FOnInsertedDiskChange;
	TDriveViewDiskChangeEvent FOnChangeDetected;
	TDriveViewDiskChangeEvent FOnChangeInvalid;
	Classes::TNotifyEvent FOnDisplayContextMenu;
	Classes::TNotifyEvent FOnRefreshDrives;
	Dirview::TDirView* FDirView;
	Fileoperator::TFileOperator* FFileOperator;
	unsigned FChangeInterval;
	System::UnicodeString FNoCheckDrives;
	Graphics::TColor FCompressedColor;
	Dirview::TFileNameDisplay FFileNameDisplay;
	System::UnicodeString FLastPathCut;
	void __fastcall SignalDirDelete(System::TObject* Sender, Classes::TStringList* Files);
	bool __fastcall CheckForSubDirs(System::UnicodeString Path);
	bool __fastcall ReadSubDirs(Comctrls::TTreeNode* Node, int DriveType);
	bool __fastcall CallBackValidateDir(Comctrls::TTreeNode* &Node, void * Data);
	bool __fastcall CallBackSaveNodeState(Comctrls::TTreeNode* &Node, void * Data);
	bool __fastcall CallBackRestoreNodeState(Comctrls::TTreeNode* &Node, void * Data);
	bool __fastcall CallBackDisplayName(Comctrls::TTreeNode* &Node, void * Data);
	bool __fastcall CallBackSetDirSize(Comctrls::TTreeNode* &Node, void * Data);
	bool __fastcall CallBackExpandLevel(Comctrls::TTreeNode* &Node, void * Data);
	void __fastcall ChangeDetected(System::TObject* Sender, const System::UnicodeString Directory, bool &SubdirsChanged);
	void __fastcall ChangeInvalid(System::TObject* Sender, const System::UnicodeString Directory, const System::UnicodeString ErrorStr);
	void __fastcall ChangeTimerOnTimer(System::TObject* Sender);
	
protected:
	HIDESBASE void __fastcall SetSelected(Comctrls::TTreeNode* Node);
	void __fastcall SetFullDriveScan(bool DoFullDriveScan);
	void __fastcall SetWatchDirectory(bool Value);
	void __fastcall SetShowDirSize(bool ShowIt);
	void __fastcall SetShowVolLabel(bool ShowIt);
	void __fastcall SetVolDisplayStyle(Dirview::TVolumeDisplayStyle DoStyle);
	void __fastcall SetDirView(Dirview::TDirView* Value);
	void __fastcall SetChangeInterval(unsigned Value);
	void __fastcall SetNoCheckDrives(System::UnicodeString Value);
	void __fastcall SetCompressedColor(Graphics::TColor Value);
	void __fastcall SetFileNameDisplay(Dirview::TFileNameDisplay Value);
	virtual void __fastcall SetDirectory(System::UnicodeString Value);
	void __fastcall SetDrive(System::WideChar Drive);
	System::WideChar __fastcall GetDrive(void);
	void __fastcall GetNodeShellAttr(_di_IShellFolder ParentFolder, TNodeData* NodeData, System::UnicodeString Path, bool ContentMask = true);
	virtual bool __fastcall DoScanDir(Comctrls::TTreeNode* FromNode);
	virtual Comctrls::TTreeNode* __fastcall AddChildNode(Comctrls::TTreeNode* ParentNode, const Sysutils::TSearchRec &SRec);
	virtual void __fastcall CreateWatchThread(System::WideChar Drive);
	void __fastcall InternalWndProc(Messages::TMessage &Msg);
	int __fastcall DirAttrMask(void);
	virtual void __fastcall ValidateDirectoryEx(Comctrls::TTreeNode* Node, Customdriveview::TRecursiveScan Recurse, bool NewDirs);
	void __fastcall ValidateDirectoryEasy(Comctrls::TTreeNode* Node);
	virtual void __fastcall RebuildTree(void);
	void __fastcall SetLastPathCut(System::UnicodeString Path);
	virtual bool __fastcall GetCanUndoCopyMove(void);
	virtual void __fastcall CreateWnd(void);
	DYNAMIC void __fastcall Edit(const tagTVITEMW &Item);
	MESSAGE void __fastcall WMUserRename(Messages::TMessage &Message);
	virtual Customdirview::TCustomDirView* __fastcall GetCustomDirView(void);
	virtual void __fastcall SetCustomDirView(Customdirview::TCustomDirView* Value);
	virtual System::UnicodeString __fastcall NodePath(Comctrls::TTreeNode* Node);
	virtual bool __fastcall NodeIsRecycleBin(Comctrls::TTreeNode* Node);
	virtual bool __fastcall NodePathExists(Comctrls::TTreeNode* Node);
	virtual Graphics::TColor __fastcall NodeColor(Comctrls::TTreeNode* Node);
	virtual Comctrls::TTreeNode* __fastcall FindPathNode(System::UnicodeString Path);
	virtual Comctrls::TTreeNode* __fastcall CreateNode(void);
	virtual Dragdrop::TDropEffectSet __fastcall DDSourceEffects(void);
	virtual void __fastcall DDChooseEffect(int KeyState, int &Effect);
	virtual bool __fastcall DragCompleteFileList(void);
	virtual Dragdrop::TDragResult __fastcall DDExecute(void);
	
public:
	__property Images;
	__property StateImages;
	__property Items = {stored=false};
	__property Selected = {write=SetSelected, stored=false};
	__property _di_IShellFolder WorkPlace = {read=FWorkPlace};
	__property Controls::TDragImageList* DragImageList = {read=FDragImageList};
	__property System::WideChar Drive = {read=GetDrive, write=SetDrive, stored=false, nodefault};
	__property System::WideChar DragDrive = {read=FDragDrive, nodefault};
	__property bool CanUndoCopyMove = {read=GetCanUndoCopyMove, nodefault};
	__property Fileoperator::TFileOperator* DDFileOperator = {read=FFileOperator};
	__property System::UnicodeString LastPathCut = {read=FLastPathCut, write=SetLastPathCut};
	DYNAMIC bool __fastcall UndoCopyMove(void);
	DYNAMIC void __fastcall EmptyClipboard(void);
	DYNAMIC bool __fastcall CopyToClipBoard(Comctrls::TTreeNode* Node);
	DYNAMIC bool __fastcall CutToClipBoard(Comctrls::TTreeNode* Node);
	DYNAMIC bool __fastcall CanPasteFromClipBoard(void);
	DYNAMIC bool __fastcall PasteFromClipBoard(System::UnicodeString TargetPath = L"");
	virtual void __fastcall PerformDragDropFileOperation(Comctrls::TTreeNode* Node, int Effect);
	TDriveStatus __fastcall GetDriveStatus(System::WideChar Drive);
	int __fastcall GetDriveTypetoNode(Comctrls::TTreeNode* Node);
	int __fastcall GetDriveType(System::WideChar Drive);
	System::WideChar __fastcall GetDriveToNode(Comctrls::TTreeNode* Node);
	System::UnicodeString __fastcall GetDriveText(System::WideChar Drive);
	void __fastcall ScanDrive(System::WideChar Drive);
	void __fastcall RefreshRootNodes(bool ScanDirectory, int dsFlags);
	System::UnicodeString __fastcall GetValidDrivesStr(void);
	void __fastcall RefreshDirSize(Comctrls::TTreeNode* Node);
	void __fastcall RefreshDriveDirSize(System::WideChar Drive);
	virtual void __fastcall SetImageIndex(Comctrls::TTreeNode* Node);
	Comctrls::TTreeNode* __fastcall FindNodeToPath(System::UnicodeString Path);
	bool __fastcall NodeVerified(Comctrls::TTreeNode* Node);
	int __fastcall NodeAttr(Comctrls::TTreeNode* Node);
	Comctrls::TTreeNode* __fastcall RootNode(Comctrls::TTreeNode* Node);
	System::UnicodeString __fastcall GetDirName(Comctrls::TTreeNode* Node);
	virtual unsigned __fastcall GetDirSize(Comctrls::TTreeNode* Node);
	virtual void __fastcall SetDirSize(Comctrls::TTreeNode* Node);
	System::UnicodeString __fastcall GetDisplayName(Comctrls::TTreeNode* Node);
	virtual bool __fastcall NodeUpdateAble(Comctrls::TTreeNode* Node);
	virtual void __fastcall ExpandLevel(Comctrls::TTreeNode* Node, int Level);
	virtual System::UnicodeString __fastcall NodePathName(Comctrls::TTreeNode* Node);
	Shlobj::PItemIDList __fastcall GetFQPIDL(Comctrls::TTreeNode* Node);
	DYNAMIC int __fastcall GetSubTreeSize(Comctrls::TTreeNode* Node);
	DYNAMIC Comctrls::TTreeNode* __fastcall CreateDirectory(Comctrls::TTreeNode* ParentNode, System::UnicodeString NewName);
	DYNAMIC bool __fastcall DeleteDirectory(Comctrls::TTreeNode* Node, bool AllowUndo);
	DYNAMIC void __fastcall DeleteSubNodes(Comctrls::TTreeNode* Node);
	__fastcall virtual TDriveView(Classes::TComponent* AOwner);
	__fastcall virtual ~TDriveView(void);
	void __fastcall SaveNodesState(Comctrls::TTreeNode* Node);
	void __fastcall RestoreNodesState(Comctrls::TTreeNode* Node);
	virtual void __fastcall DisplayContextMenu(Comctrls::TTreeNode* Node, const Types::TPoint &Point);
	virtual void __fastcall DisplayPropertiesMenu(Comctrls::TTreeNode* Node);
	virtual void __fastcall StartWatchThread(void);
	virtual void __fastcall StopWatchThread(void);
	virtual void __fastcall TerminateWatchThread(System::WideChar Drive);
	virtual void __fastcall StartAllWatchThreads(void);
	virtual void __fastcall StopAllWatchThreads(void);
	bool __fastcall WatchThreadActive(void)/* overload */;
	bool __fastcall WatchThreadActive(System::WideChar Drive)/* overload */;
	virtual bool __fastcall NodeWatched(Comctrls::TTreeNode* Node);
	virtual void __fastcall GetImageIndex(Comctrls::TTreeNode* Node);
	DYNAMIC bool __fastcall CanEdit(Comctrls::TTreeNode* Node);
	DYNAMIC bool __fastcall CanChange(Comctrls::TTreeNode* Node);
	DYNAMIC bool __fastcall CanExpand(Comctrls::TTreeNode* Node);
	DYNAMIC void __fastcall Delete(Comctrls::TTreeNode* Node);
	virtual void __fastcall Loaded(void);
	DYNAMIC void __fastcall KeyPress(System::WideChar &Key);
	DYNAMIC void __fastcall Change(Comctrls::TTreeNode* Node);
	
__published:
	__property Directory;
	__property bool ConfirmDelete = {read=FConfirmDelete, write=FConfirmDelete, default=1};
	__property bool ConfirmOverwrite = {read=FConfirmOverwrite, write=FConfirmOverwrite, default=1};
	__property bool FullDriveScan = {read=FFullDriveScan, write=SetFullDriveScan, default=0};
	__property bool WatchDirectory = {read=FWatchDirectory, write=SetWatchDirectory, default=0};
	__property unsigned ChangeInterval = {read=FChangeInterval, write=SetChangeInterval, default=1000};
	__property Dirview::TDirView* DirView = {read=FDirView, write=SetDirView};
	__property bool ShowDirSize = {read=FShowDirSize, write=SetShowDirSize, default=0};
	__property bool ShowVolLabel = {read=FShowVolLabel, write=SetShowVolLabel, default=1};
	__property Dirview::TVolumeDisplayStyle VolDisplayStyle = {read=FVolDisplayStyle, write=SetVolDisplayStyle, default=0};
	__property bool ShowAnimation = {read=FShowAnimation, write=FShowAnimation, default=0};
	__property System::UnicodeString NoCheckDrives = {read=FNoCheckDrives, write=SetNoCheckDrives};
	__property Graphics::TColor CompressedColor = {read=FCompressedColor, write=SetCompressedColor, default=16711680};
	__property Dirview::TFileNameDisplay FileNameDisplay = {read=FFileNameDisplay, write=SetFileNameDisplay, default=0};
	__property Classes::TNotifyEvent OnStartScan = {read=FOnStartScan, write=FOnStartScan};
	__property Classes::TNotifyEvent OnEndScan = {read=FOnEndScan, write=FOnEndScan};
	__property TDriveViewScanDirEvent OnScanDir = {read=FOnScanDir, write=FOnScanDir};
	__property TDriveViewDiskChangeEvent OnDiskChange = {read=FOnDiskChange, write=FOnDiskChange};
	__property TDriveViewDiskChangeEvent OnInsertedDiskChange = {read=FOnInsertedDiskChange, write=FOnInsertedDiskChange};
	__property TDriveViewDiskChangeEvent OnChangeDetected = {read=FOnChangeDetected, write=FOnChangeDetected};
	__property TDriveViewDiskChangeEvent OnChangeInvalid = {read=FOnChangeInvalid, write=FOnChangeInvalid};
	__property Classes::TNotifyEvent OnDisplayContextMenu = {read=FOnDisplayContextMenu, write=FOnDisplayContextMenu};
	__property Classes::TNotifyEvent OnRefreshDrives = {read=FOnRefreshDrives, write=FOnRefreshDrives};
	__property DDLinkOnExeDrag = {default=1};
	__property UseDragImages = {default=1};
	__property TargetPopUpMenu = {default=1};
	__property OnDDDragEnter;
	__property OnDDDragLeave;
	__property OnDDDragOver;
	__property OnDDDrop;
	__property OnDDQueryContinueDrag;
	__property OnDDGiveFeedback;
	__property OnDDDragDetect;
	__property OnDDProcessDropped;
	__property OnDDError;
	__property OnDDExecuted;
	__property OnDDFileOperation;
	__property OnDDFileOperationExecuted;
	__property OnDDMenuPopup;
	__property Align = {default=0};
	__property Anchors = {default=3};
	__property AutoExpand = {default=0};
	__property BiDiMode;
	__property BorderStyle = {default=1};
	__property BorderWidth = {default=0};
	__property ChangeDelay = {default=0};
	__property Color = {default=-16777211};
	__property Ctl3D;
	__property Constraints;
	__property DragKind = {default=0};
	__property DragCursor = {default=-12};
	__property DragMode = {default=1};
	__property OnDragDrop;
	__property OnDragOver;
	__property Enabled = {default=1};
	__property Font;
	__property HideSelection = {default=1};
	__property HotTrack = {default=0};
	__property Indent;
	__property ParentBiDiMode = {default=1};
	__property ParentColor = {default=1};
	__property ParentCtl3D = {default=1};
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ReadOnly = {default=0};
	__property RightClickSelect = {default=0};
	__property RowSelect = {default=0};
	__property ShowButtons = {default=1};
	__property ShowHint;
	__property ShowLines = {default=1};
	__property TabOrder = {default=-1};
	__property TabStop = {default=1};
	__property ToolTips = {default=1};
	__property Visible = {default=1};
	__property OnChange;
	__property OnChanging;
	__property OnClick;
	__property OnCollapsing;
	__property OnCollapsed;
	__property OnCompare;
	__property OnDblClick;
	__property OnDeletion;
	__property OnEdited;
	__property OnEditing;
	__property OnEndDock;
	__property OnEndDrag;
	__property OnEnter;
	__property OnExit;
	__property OnExpanding;
	__property OnExpanded;
	__property OnGetImageIndex;
	__property OnGetSelectedIndex;
	__property OnKeyDown;
	__property OnKeyPress;
	__property OnKeyUp;
	__property OnMouseDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnStartDock;
	__property OnStartDrag;
public:
	/* TWinControl.CreateParented */ inline __fastcall TDriveView(HWND ParentWindow) : Customdriveview::TCustomDriveView(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE System::ResourceString _coFileOperatorTitle;
#define Driveview_coFileOperatorTitle System::LoadResourceString(&Driveview::_coFileOperatorTitle)
extern PACKAGE System::ResourceString _coInvalidDosChars;
#define Driveview_coInvalidDosChars System::LoadResourceString(&Driveview::_coInvalidDosChars)
extern PACKAGE System::ResourceString _Space;
#define Driveview_Space System::LoadResourceString(&Driveview::_Space)
static const ShortInt msThreadChangeDelay = 0x32;
static const unsigned CInvalidSize = 0xffffffff;
#define ErrorNodeNA L"%s: Node not assigned"
static const ShortInt dvdsFloppy = 0x8;
static const ShortInt dvdsRereadAllways = 0x10;
extern PACKAGE void __fastcall Register(void);

}	/* namespace Driveview */
using namespace Driveview;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DriveviewHPP
