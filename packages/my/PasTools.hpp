// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Pastools.pas' rev: 21.00

#ifndef PastoolsHPP
#define PastoolsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Types.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Comctrls.hpp>	// Pascal unit
#include <Extctrls.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit
#include <Dialogs.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Pastools
{
//-- type declarations -------------------------------------------------------
typedef void __fastcall (__closure *TControlScrollBeforeUpdate)(System::TObject* ObjectToValidate);

typedef void __fastcall (__closure *TControlScrollAfterUpdate)(void);

class DELPHICLASS TCustomControlScrollOnDragOver;
class PASCALIMPLEMENTATION TCustomControlScrollOnDragOver : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TControlScrollBeforeUpdate FOnBeforeUpdate;
	TControlScrollAfterUpdate FOnAfterUpdate;
	Extctrls::TTimer* FDragOverTimer;
	Controls::TControl* FControl;
	_FILETIME FDragOverTime;
	_FILETIME FLastVScrollTime;
	int FVScrollCount;
	void __fastcall DragOverTimer(System::TObject* Sender);
	void __fastcall BeforeUpdate(System::TObject* ObjectToValidate);
	void __fastcall AfterUpdate(void);
	
public:
	__fastcall TCustomControlScrollOnDragOver(Controls::TControl* Control, bool ScheduleDragOver);
	__fastcall virtual ~TCustomControlScrollOnDragOver(void);
	virtual void __fastcall StartDrag(void);
	virtual void __fastcall EndDrag(void);
	virtual void __fastcall DragOver(const Types::TPoint &Point) = 0 ;
	__property TControlScrollBeforeUpdate OnBeforeUpdate = {read=FOnBeforeUpdate, write=FOnBeforeUpdate};
	__property TControlScrollAfterUpdate OnAfterUpdate = {read=FOnAfterUpdate, write=FOnAfterUpdate};
};


class DELPHICLASS TTreeViewScrollOnDragOver;
class PASCALIMPLEMENTATION TTreeViewScrollOnDragOver : public TCustomControlScrollOnDragOver
{
	typedef TCustomControlScrollOnDragOver inherited;
	
private:
	Comctrls::TTreeNode* FLastDragNode;
	_FILETIME FLastHScrollTime;
	
public:
	virtual void __fastcall StartDrag(void);
	virtual void __fastcall DragOver(const Types::TPoint &Point);
public:
	/* TCustomControlScrollOnDragOver.Create */ inline __fastcall TTreeViewScrollOnDragOver(Controls::TControl* Control, bool ScheduleDragOver) : TCustomControlScrollOnDragOver(Control, ScheduleDragOver) { }
	/* TCustomControlScrollOnDragOver.Destroy */ inline __fastcall virtual ~TTreeViewScrollOnDragOver(void) { }
	
};


class DELPHICLASS TListViewScrollOnDragOver;
class PASCALIMPLEMENTATION TListViewScrollOnDragOver : public TCustomControlScrollOnDragOver
{
	typedef TCustomControlScrollOnDragOver inherited;
	
public:
	virtual void __fastcall DragOver(const Types::TPoint &Point);
public:
	/* TCustomControlScrollOnDragOver.Create */ inline __fastcall TListViewScrollOnDragOver(Controls::TControl* Control, bool ScheduleDragOver) : TCustomControlScrollOnDragOver(Control, ScheduleDragOver) { }
	/* TCustomControlScrollOnDragOver.Destroy */ inline __fastcall virtual ~TListViewScrollOnDragOver(void) { }
	
};


class DELPHICLASS TListBoxScrollOnDragOver;
class PASCALIMPLEMENTATION TListBoxScrollOnDragOver : public TCustomControlScrollOnDragOver
{
	typedef TCustomControlScrollOnDragOver inherited;
	
public:
	virtual void __fastcall DragOver(const Types::TPoint &Point);
public:
	/* TCustomControlScrollOnDragOver.Create */ inline __fastcall TListBoxScrollOnDragOver(Controls::TControl* Control, bool ScheduleDragOver) : TCustomControlScrollOnDragOver(Control, ScheduleDragOver) { }
	/* TCustomControlScrollOnDragOver.Destroy */ inline __fastcall virtual ~TListBoxScrollOnDragOver(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE Classes::TComponent* __fastcall Construct(Classes::TComponentClass ComponentClass, Classes::TComponent* Owner);
extern PACKAGE bool __fastcall IsVista(void);
extern PACKAGE bool __fastcall IsExactly2008R2(void);
extern PACKAGE System::UnicodeString __fastcall CutToChar(System::UnicodeString &Str, System::WideChar Ch, bool Trim);
extern PACKAGE void __fastcall FilterToFileTypes(System::UnicodeString Filter, Dialogs::TFileTypeItems* FileTypes);

}	/* namespace Pastools */
using namespace Pastools;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// PastoolsHPP
