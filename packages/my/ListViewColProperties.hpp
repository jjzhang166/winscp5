// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Listviewcolproperties.pas' rev: 21.00

#ifndef ListviewcolpropertiesHPP
#define ListviewcolpropertiesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Comctrls.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Listviewcolproperties
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TCustomListViewColProperty;
class PASCALIMPLEMENTATION TCustomListViewColProperty : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	Classes::TAlignment Alignment;
	System::UnicodeString Caption;
	int Width;
	bool Visible;
	int Order;
	__fastcall TCustomListViewColProperty(int AOrder);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TCustomListViewColProperty(void) { }
	
};


class DELPHICLASS TCustomListViewColProperties;
class PASCALIMPLEMENTATION TCustomListViewColProperties : public Classes::TPersistent
{
	typedef Classes::TPersistent inherited;
	
private:
	bool FChanged;
	int FMaxWidth;
	int FMinWidth;
	Classes::TNotifyEvent FOnChange;
	int FUpdating;
	Classes::TList* FProperties;
	bool FCreated;
	Comctrls::TListColumns* __fastcall GetColumns(void);
	int __fastcall GetCount(void);
	System::UnicodeString __fastcall GetOrderStr(void);
	void __fastcall CheckBounds(int Index);
	void __fastcall SetMaxWidth(int Value);
	void __fastcall SetMinWidth(int Value);
	void __fastcall SetWidthsStr(System::UnicodeString Value);
	System::UnicodeString __fastcall GetWidthsStr(void);
	void __fastcall SetOrderStr(System::UnicodeString Value);
	
protected:
	Comctrls::TCustomListView* FListView;
	bool FListViewManaged;
	Classes::TAlignment __fastcall GetAlignments(int Index);
	int __fastcall GetOrder(int Index);
	virtual System::UnicodeString __fastcall GetParamsStr(void);
	bool __fastcall GetVisible(int Index);
	int __fastcall GetWidths(int Index);
	void __fastcall SetAlignments(int Index, Classes::TAlignment Value);
	void __fastcall SetVisible(int Index, bool Value);
	void __fastcall SetWidths(int Index, int Value);
	void __fastcall SetOrder(int Index, int Value);
	System::UnicodeString __fastcall GetCaptions(int Index);
	virtual void __fastcall Changed(void);
	virtual void __fastcall SetCaptions(int Index, System::UnicodeString Value);
	virtual void __fastcall SetParamsStr(System::UnicodeString Value);
	void __fastcall UpdateListView(void);
	void __fastcall UpdateFromListView(void);
	TCustomListViewColProperty* __fastcall GetProperties(int Index);
	bool __fastcall ColumnsExists(void);
	void __fastcall SetRuntimeVisible(int Index, bool Value, bool SaveWidth);
	Comctrls::TListColumn* __fastcall GetColumn(int Index);
	void __fastcall CreateProperties(int ACount);
	__property Comctrls::TListColumns* Columns = {read=GetColumns, stored=false};
	__property int Count = {read=GetCount, stored=false, nodefault};
	
public:
	__fastcall TCustomListViewColProperties(Comctrls::TCustomListView* ListView, int ColCount);
	void __fastcall EndUpdate(void);
	void __fastcall BeginUpdate(void);
	void __fastcall ListViewWndCreated(void);
	void __fastcall ListViewWndDestroying(void);
	void __fastcall ListViewWndDestroyed(void);
	__property Classes::TAlignment Alignments[int Index] = {read=GetAlignments, write=SetAlignments};
	__property System::UnicodeString Captions[int Index] = {read=GetCaptions, write=SetCaptions};
	__property int MaxWidth = {read=FMaxWidth, write=SetMaxWidth, default=1000};
	__property int MinWidth = {read=FMinWidth, write=SetMinWidth, default=20};
	__property int Widths[int Index] = {read=GetWidths, write=SetWidths};
	__property bool Visible[int Index] = {read=GetVisible, write=SetVisible};
	void __fastcall RecreateColumns(void);
	__property Classes::TNotifyEvent OnChange = {read=FOnChange, write=FOnChange};
	__property int Order[int Index] = {read=GetOrder, write=SetOrder};
	__property System::UnicodeString OrderStr = {read=GetOrderStr, write=SetOrderStr, stored=false};
	__property System::UnicodeString ParamsStr = {read=GetParamsStr, write=SetParamsStr, stored=false};
	__property System::UnicodeString WidthsStr = {read=GetWidthsStr, write=SetWidthsStr, stored=false};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TCustomListViewColProperties(void) { }
	
};


class DELPHICLASS TListViewColProperties;
class PASCALIMPLEMENTATION TListViewColProperties : public TCustomListViewColProperties
{
	typedef TCustomListViewColProperties inherited;
	
__published:
	__property MaxWidth = {default=1000};
	__property MinWidth = {default=20};
public:
	/* TCustomListViewColProperties.Create */ inline __fastcall TListViewColProperties(Comctrls::TCustomListView* ListView, int ColCount) : TCustomListViewColProperties(ListView, ColCount) { }
	
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TListViewColProperties(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
static const Word DefaultListViewMaxWidth = 0x3e8;
static const ShortInt DefaultListViewMinWidth = 0x14;

}	/* namespace Listviewcolproperties */
using namespace Listviewcolproperties;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// ListviewcolpropertiesHPP
