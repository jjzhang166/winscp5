// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Historycombobox.pas' rev: 21.00

#ifndef HistorycomboboxHPP
#define HistorycomboboxHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Messages.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Dialogs.hpp>	// Pascal unit
#include <Stdctrls.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Historycombobox
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum Historycombobox__1 { soExit, soDropDown };
#pragma option pop

typedef Set<Historycombobox__1, soExit, soDropDown>  THistorySaveOn;

class DELPHICLASS THistoryComboBox;
typedef void __fastcall (__closure *THistoryComboBoxGetData)(THistoryComboBox* Sender, void * &Data);

typedef void __fastcall (__closure *THistoryComboBoxSetData)(THistoryComboBox* Sender, void * Data);

class PASCALIMPLEMENTATION THistoryComboBox : public Stdctrls::TComboBox
{
	typedef Stdctrls::TComboBox inherited;
	
private:
	THistorySaveOn FSaveOn;
	int FMaxHistorySize;
	THistoryComboBoxGetData FOnGetData;
	THistoryComboBoxSetData FOnSetData;
	void __fastcall SetMaxHistorySize(int AMaxHistorySize);
	bool __fastcall StoreSaveOn(void);
	int __fastcall GetMaxItemWidth(void);
	
protected:
	DYNAMIC void __fastcall DoExit(void);
	DYNAMIC void __fastcall DropDown(void);
	DYNAMIC void __fastcall KeyDown(System::Word &Key, Classes::TShiftState Shift);
	DYNAMIC void __fastcall Change(void);
	
public:
	__fastcall virtual THistoryComboBox(Classes::TComponent* AOwner);
	virtual void __fastcall SaveToHistory(void);
	
__published:
	__property THistorySaveOn SaveOn = {read=FSaveOn, write=FSaveOn, stored=StoreSaveOn, nodefault};
	__property int MaxHistorySize = {read=FMaxHistorySize, write=SetMaxHistorySize, default=30};
	__property THistoryComboBoxGetData OnGetData = {read=FOnGetData, write=FOnGetData};
	__property THistoryComboBoxSetData OnSetData = {read=FOnSetData, write=FOnSetData};
public:
	/* TCustomComboBox.Destroy */ inline __fastcall virtual ~THistoryComboBox(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall THistoryComboBox(HWND ParentWindow) : Stdctrls::TComboBox(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
#define DefaultHistorySaveOn (Set<Historycombobox__1, soExit, soDropDown> () << soExit << soDropDown )
static const ShortInt DefaultMaxHistorySize = 0x1e;
extern PACKAGE void __fastcall Register(void);
extern PACKAGE void __fastcall SaveToHistory(Classes::TStrings* Strings, System::UnicodeString T, void * Data = (void *)(0x0), int MaxHistorySize = 0x1e);

}	/* namespace Historycombobox */
using namespace Historycombobox;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// HistorycomboboxHPP
